//Edited By: Tyler Arruda


import 'package:flutter/material.dart';
import 'package:soldier_companion/Widgets/eventRequestList.dart';
import 'package:soldier_companion/Models/soldier.dart';

class EventRequest extends StatefulWidget{

  Soldier soldier;

  EventRequest(this.soldier);

  @override
  State<StatefulWidget> createState() => new _EventRequestState(soldier);

}

class _EventRequestState extends State<EventRequest>{

  Soldier soldier;

  _EventRequestState(this.soldier);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text('Event Requests'),
        ),
        body: Center(
            child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.all(40.0),
                child: Column(
                  children: <Widget>[
                    new EventRequestList(soldier),
                  ],
                ))));
  }

}