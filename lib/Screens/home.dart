//Edited By: Tyler Arruda

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:soldier_companion/Screens/documentRequest.dart';
import 'package:soldier_companion/Screens/signUp.dart';
import 'package:soldier_companion/Screens/eventRequests.dart';
import 'package:soldier_companion/Models/soldier.dart';


class Home extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => new _HomeState();

}

class _HomeState extends State<Home> {

  String soldierNumber = "A70000000";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Dashboard'),
        ),
        body: Center(
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(40.0),
              child: Column(
                children: <Widget>[
                  new RaisedButton(
                    child: Text('Event Requests'),
                    onPressed: () => openEventRequests(context, soldierNumber),
                  ),
                  new RaisedButton(
                    child: Text('Soldier Sign Up'),
                    onPressed: () => openSoldierSignUp(context),
                  ),
                  new RaisedButton(
                    child: Text('View Document Requests'),
                    onPressed: () => openDocumentRequest(context),
                  ),
                  new RaisedButton(
                    child: Text('Create Document Request'),
                    onPressed: () => openSoldierSignUp(context),
                  ),
                ],
              ),
            )));
  }

  Future openSoldierSignUp(BuildContext context) async {
    soldierNumber = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SignUp()),
    );

  }

  void openDocumentRequest(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => DocumentRequest()),
    );
  }

  void openEventRequests(BuildContext context, String soldierNumber) {
    Soldier currentSoldier = new Soldier();

    Firestore.instance.collection('soldiers').where(
        'sn', isEqualTo: soldierNumber).snapshots().listen(
            (data) =>
        {
        currentSoldier.soldierDocID = data.documents[0]["soldierDocID"],
        currentSoldier.attendingFirestoreArray = data.documents[0]["attending"],
        print(data.documents[0]["soldierDocID"]),
        currentSoldier.name = data.documents[0]["name"],
        currentSoldier.sodierNumber = soldierNumber,
            () => _makeArray(currentSoldier),
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => EventRequest(currentSoldier)),
        ),
        }
    );
  }

  void _makeArray(Soldier soldier) {
    var arrayLength = soldier.attendingFirestoreArray.length;
    for (int x = 0; x < arrayLength; x++) {
      soldier.attending.add(soldier.attendingFirestoreArray[x]);
    }
  }
}