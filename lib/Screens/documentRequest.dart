//Edited By: Tyler Arruda


import 'package:flutter/material.dart';
import "package:soldier_companion/Widgets/documentRequestList.dart";


class DocumentRequest extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => new _DocumentRequestState();
  
}

class _DocumentRequestState extends State<DocumentRequest>{

  //Soldier Id
  String soldierId;

  @override
  void initState() {
    // Firestore.instance.collection('documentRequests').document()
    // .setData({ 'DownloadURL': 'test', 'FormName': 'test' });

    //String To hold Soldiers ID
    //TODO: Get the soldier ID through authentication or through user input
    soldierId = "A00000001";

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
        title: Text('Document Signing Requests'),
      ),
      body: Center(
        child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.all(40.0),
            child: Column(
              children: <Widget>[
                new DocumentRequestList(soldierId),
              ],
            ))));
  }
}