//Edited By: Tyler Arruda

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:soldier_companion/Models/soldier.dart';

class SignUp extends StatefulWidget{
  final String title = "Sign Up Page";
  @override
  State<StatefulWidget> createState() => new _SignUpState();

}

class _SignUpState extends State<SignUp>{
  @override

  void initState(){
    super.initState();
  }

  void dispose(){
    soldierNumController.dispose();
    nameController.dispose();
    super.dispose();
  }

  final soldierNumController = TextEditingController();
  final nameController = TextEditingController();

  List<String> qualificationsList = [];
  var fourofoursList = [];

  Soldier soldierToSignUp = new Soldier();

  String lsvTitle = "LSVW";
  String mlvwTitle = "MLVW";
  String milcotsTitle = "MilCOTS";
  String c3Title = "C3";
  String lglTitle = "LG1";
  String m777Title = "M777";
  String arsoTitle = "small arms ARSO";


  bool chkLsvw = false;
  bool chkMlvw = false;
  bool chkMilcots = false;
  bool chkC3 = false;
  bool chkLg1 = false;
  bool chkM777 = false;
  bool chkArso = false;



  void _LsvwChanged(bool value) => setState(() => chkLsvw = value);
  void _MlvwChanged(bool value) => setState(() => chkMlvw = value);
  void _MilcotsChanged(bool value) => setState(() => chkMilcots = value);
  void _C3Changed(bool value) => setState(() => chkC3 = value);
  void _Lg1Changed(bool value) => setState(() => chkLg1 = value);
  void _M777Changed(bool value) => setState(() => chkM777 = value);
  void _ArsoChanged(bool value) => setState(() => chkArso = value);

  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
        actions: <Widget>[
          new IconButton(icon: const Icon(Icons.save), onPressed: () => pushSoldier(soldierToSignUp))
        ],
      ),
      body: new ListView(
        shrinkWrap: true,
        children: <Widget>[
          const Divider(
            height: 10.0,
            color: Colors.blue,
          ),
          new ListTile(
            title: new Center(child: const Text('Soldier Information')),
          ),
          new ListTile(
            leading: const Icon(Icons.person),
            title: new TextField(
              controller: soldierNumController,
              decoration: new InputDecoration(
                hintText: "Soldier Number",
              ),
            ),
          ),
          new ListTile(
            leading: const Icon(Icons.person),
            title: new TextField(
              controller: nameController,
              decoration: new InputDecoration(
                hintText: "Name",
              ),
            ),
          ),
          const Divider(
            height: 10.0,
            color: Colors.blue,
          ),
          new ListTile(
            title: new Center(child: const Text('404s')),
          ),
          CheckboxListTile(
            title: const Text('LSVW'),
            value: chkLsvw,
            secondary: const Icon(Icons.directions_car),
            onChanged: _LsvwChanged,
          ),
          CheckboxListTile(
            title: const Text('MLVW'),
            value: chkMlvw,
            secondary: const Icon(Icons.directions_car),
            onChanged: _MlvwChanged,
          ),
          CheckboxListTile(
            title: const Text('MilCOTS'),
            value: chkMilcots,
            secondary: const Icon(Icons.directions_car),
            onChanged: _MilcotsChanged,
          ),
          const Divider(
            height: 10.0,
            color: Colors.blue,
          ),
          new ListTile(
            title: new Center(child: const Text('Qualifications')),
          ),
          CheckboxListTile(
            title: const Text('C3'),
            value: chkC3,
            onChanged: _C3Changed,
            secondary: const Icon(Icons.insert_drive_file),
          ),
          CheckboxListTile(
            title: const Text('LG1'),
            value: chkLg1,
            onChanged: _Lg1Changed,
            secondary: const Icon(Icons.insert_drive_file),
          ),
          CheckboxListTile(
            title: const Text('M777'),
            value: chkM777,
            onChanged: _M777Changed,
            secondary: const Icon(Icons.insert_drive_file),
          ),
          CheckboxListTile(
            title: const Text('small arms ARSO'),
            value: chkArso,
            onChanged: _ArsoChanged,
            secondary: const Icon(Icons.insert_drive_file),
          ),
          RaisedButton(
            child: Text('Create Soldier'),
            color: Colors.blue,
            onPressed: () => pushSoldier(soldierToSignUp),
          ),
        ],
      ),
    );
  }


  void pushSoldier(Soldier soldier){

    fourofoursList = [];
    qualificationsList = [];

    buildQualifications(soldier);
    buildFourofours(soldier);

    //Convert the data into firebase readable values
     Firestore.instance.collection('soldiers').document(soldierNumController.text)
     .setData({ 'attending': ['range-weekend'], 'qualifications': qualificationsList, 'name': nameController.text, 'fourzerofours': fourofoursList, 'sn': soldierNumController.text, 'fourzerofoursDate': new DateTime.now(), 'rank': 'MCpl'});

     Navigator.pop(context, soldierNumController.text);

  }

  void buildQualifications(Soldier soldier){
    if(chkLg1){qualificationsList.add(lglTitle);}
    if(chkM777){qualificationsList.add(m777Title);}
    if(chkC3){qualificationsList.add(c3Title);}
    if(chkArso){qualificationsList.add(arsoTitle);}
  }

  void buildFourofours(Soldier soldier){
    if(chkLsvw){fourofoursList.add(lsvTitle);}
    if(chkMlvw){fourofoursList.add(mlvwTitle);}
    if(chkMilcots){fourofoursList.add(milcotsTitle);}
  }


}