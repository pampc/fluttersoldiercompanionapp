import 'package:flutter/material.dart';
import './Screens/home.dart';

void main() => runApp(FormPull());

class FormPull extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Form Pulling',
      home: Scaffold(
          body: Home()
      ),
    );
  }
}