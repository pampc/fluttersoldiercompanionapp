//Edited By: Tyler Arruda

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class DocumentRequestList extends StatelessWidget {

  final String soldierId;

  DocumentRequestList(this.soldierId);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection('documentRequests').where('SoldierID', isEqualTo: soldierId).snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError)
          return new Text('Error: ${snapshot.error}');
        switch (snapshot.connectionState) {
          case ConnectionState.waiting: return new Text('Loading...');
          default:
            return new Flexible(
                child: ListView(
                  children: snapshot.data.documents.map((DocumentSnapshot document) {
                    return new ListTile(
                      title: new Text(document['FormName']),
                      subtitle: RaisedButton(
                        elevation: 4.0,
                        splashColor: Colors.cyan,
                        onPressed: () => _launchURL(document['DownloadURL']),
                        child: Text(
                          "Download PDF",
                          style: TextStyle(decoration: TextDecoration.none),
                        ),
                      ),
                    );
                  }).toList(),
                )
            );
        }
      },
    );
  }

  _launchURL(String downloadUrl) async {
    //const url = 'https://firebasestorage.googleapis.com/v0/b/prototypetesting-9e43d.appspot.com/o/USAR-Team-Fact-Sheet.pdf?alt=media&token=85ca1400-057e-4fe0-906f-0c17491f1702';
    if (await canLaunch(downloadUrl)) {
      await launch(downloadUrl);
    } else {
      throw 'Could not launch $downloadUrl';
    }
  }
}