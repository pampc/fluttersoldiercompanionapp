//Edited By: Tyler Arruda

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:soldier_companion/Models/soldier.dart';

class EventRequestList extends StatelessWidget {

  Soldier soldier;

  EventRequestList(this.soldier);

  List<String> attending;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection('event').snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError)
          return new Text('Error: ${snapshot.error}');
        switch (snapshot.connectionState) {
          case ConnectionState.waiting: return new Text('Loading...');
          default:
            return new Flexible(
                child: ListView(
                  children: snapshot.data.documents.map((DocumentSnapshot document) {
                    return new
                    ListTile(
                      title: new Text(document["title"]),
                      subtitle: RaisedButton(
                        elevation: 4.0,
                        splashColor: Colors.cyan,
                        onPressed: () => updateAttendance(document["slug"], context),
                        child: Text("Respond to Event"),
                      ),
                    );
                  }).toList(),
                )
            );
        }
      },
    );
  }


   updateAttendance(String exercise, BuildContext context) {
      if(soldier.attending != null){
        soldier.attending.add(exercise);
      }else soldier.attending = [exercise];

      Firestore.instance.collection("soldiers").document(soldier.sodierNumber).updateData({'attending': soldier.attending});

      Navigator.pop(context);
  }

  String checkCurrentStatus(String slug) {

  }


}